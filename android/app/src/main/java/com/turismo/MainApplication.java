package com.turismo;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.image.zoom.ReactImageZoom;
import com.reactnative.photoview.PhotoViewPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.horcrux.svg.SvgPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.i18n.reactnativei18n.ReactNativeI18n;
import com.reactlibrary.RNCardViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;
import com.reactnativenavigation.NavigationApplication;

 public class MainApplication extends NavigationApplication {

     @Override
     public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     protected List<ReactPackage> getPackages() {
         // Add additional packages you require here
         // No need to add RnnPackage and MainReactPackage
         return Arrays.<ReactPackage>asList(
            // new MainReactPackage(),
            new ReactImageZoom(),
            new PhotoViewPackage(),
            new MapsPackage(),
            new SvgPackage(),
            new RNSpinkitPackage(),
            new ReactNativeI18n(),
            new RNCardViewPackage()
         );
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
         return getPackages();
     }
 }
