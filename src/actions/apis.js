import axios from 'axios';

export const url = 'http://test.backoffice.sanpedrosula.hn/';
// export const url = 'http://local.backoffice.com/'

const backoffice = axios.create({
  baseURL: url,
  headers : {
    'Content-Type' : 'application/json',
  },
});

const openWeather = axios.create({
  baseURL: 'http://api.openweathermap.org/data/2.5/weather?q=San Pedro Sula,hn&appid=b0aa2592933549971eab6509e2faf7d4',
  headers : {
    'Content-Type' : 'application/json',
  },
})

const foursquare  = axios.create({
  baseURL: 'https://api.foursquare.com/v2/',

  headers : {
    'Content-Type' : 'application/json',
  },
});

const client_id = 'TU1V24PT4CLZBK0EUZXKZFWQSYK5FCVDRQIZY2NI0XIUYGOQ';
const client_secret = 'F3GNZ2S41QE3VWKEUYZI3L4BD3DDVKC2MKWXZ3IOXLWET4LO'

export default {
  weather : async () => {
    try {
      const weather = await openWeather.get();
      return {
        cod: weather.data.weather[0].id,
        temp: weather.data.main.temp - 273.15
      }
    } catch (e) {
      return {};
    }
  },
  news                     : async () => {
    try{
      const results = await backoffice.get('api/news');
      return results.data.data
    } catch (e) {
      return {};
    }
  },
  events                    : async (id, history) => {
    try { 
      const t = history  ? 'history' : 'current';
      const results = await backoffice.get(`api/event?category_id=${id}&date=${t}`);
      return {
        data: results.data.data,
        category: id,
        type: t
      };
    } catch(e) {
      return {};
    }
  },
  eventCategories          : async () => {
    try {
      const results = await backoffice.get('api/event-category');
      return results.data.data;
    } catch (e) {
      console.log(e)
      return {};
    }
  },
  routes                   : async category => {
    try { 
      const results = await backoffice.get(`api/transportation-route?category_id=${category}`);
      console.log(results)
      return {
        data: results.data.data,
        category
      };
    } catch (e) {
      console.log(e)
      return {};
    }
  },
  routeCategories          : async type => {
    try { 
      const results = await backoffice.get(`api/route-category?type=${type}`);
      return {
        [type]: results.data.data
      };
    } catch (e) {
      return {};
    }
  },
  routeCompaniesCategories : async () => {
    try{
      const results = await backoffice.get('api/transportation-company-category');
      return results.data.data;
    } catch (e) {
      console.log(e)
      return {};
    }
  },
  phones          : async () => {
    try{
      const results = await backoffice.get('api/emergency-phone');
      console.log(results)
      return results.data.data;
    } catch (e) {
      console.log(e)
      return {};
    }
  },
  transportationCompanies  : async category => {
    try { 
      const results = await backoffice.get(`api/transportation-company?category_id=${category}`);
      return {
        data: results.data.data,
        category,
      };
    } catch (e) {
      return {};
    }
  },
  places                   : async category => {
    try {
      const results = await foursquare.get(`venues/search/?near=San Pedro Sula&radius=4000&categoryId=${category}&client_id=${client_id}&client_secret=${client_secret}&v=20131124`)
      const venues = results.data.response.venues;
      
      return {
        category,
        data: venues
      }
    } catch (e) {
      console.log(e);
      return []
    }
  },
  venuePhotos             : async (venueId, category, index) => {
    try {
      const photos = await foursquare.get(`venues/${venueId}/photos?client_id=${client_id}&client_secret=${client_secret}&v=20131124`);
      
      return {
        photos: photos.data.response.photos.items,
        venueId,
        category, 
        index
      }
    } catch(e) {
      return [];
    }
  },
  placeCategories          : async () => backoffice.get('place-categories'),
};
