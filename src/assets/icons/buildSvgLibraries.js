const SvgToAndroid = require('node-svg2android');
const jsondir = require('jsondir');
const _ = require('lodash');
const parser = require('xml2json');
const jsonfile = require('jsonfile');

const converter = new SvgToAndroid();
const iconsPath = './src/assets/icons';

function finishRun () {
  converter.end();
  process.exit();
}

function formatTree (jsonDir) {
  const retArr = [];
  _.forEach(jsonDir, (directory, directoryName) => {
    if (directory['-type'] === 'd') {
      const dirFiles = [];
      _.forEach(directory, (svgFile, fileName) => {
        if (fileName.indexOf('svg') >= 0) {
          const name = fileName.replace('.svg', '');
          dirFiles.push({name, svgData : svgFile['-content']});
        }
      });
      if (dirFiles.length > 0) {
        retArr.push({
          name  : directoryName,
          files : dirFiles,
        });
      }
    }
  });
  return retArr;
}

function getDirData () {
  return new Promise((resolve, reject) => {
    jsondir.dir2json(iconsPath, (err, results) => {
      if (err) {
        reject(err);
      }
      resolve(formatTree(results));
    });
  });
}

function convertAndroidToJson (androidSvgCode) {
  const xmlJson = parser.toJson(androidSvgCode, {object : true});
  const output = {};
  if (xmlJson.vector) {
    output.viewBox = [parseFloat(xmlJson.vector['android:viewportWidth']), parseFloat(xmlJson.vector['android:viewportHeight'])];
    if (_.isArray(xmlJson.vector.path) || xmlJson.vector.path.length) {
      output.paths = xmlJson.vector.path.map((path) => {
        return path['android:pathData'];
      });
    } else {
      output.paths = [xmlJson.vector.path['android:pathData']];
    }
  }
  return output;
}

function convertFileToAndroid (path, name) {
  return new Promise((resolve, reject) => {

    converter.convert(path).then((androidSvgCode) => {
      if (androidSvgCode.error === '') {
        const jsonAndroidVector = {
          name,
          svgData : convertAndroidToJson(androidSvgCode.code),
        };
        resolve(jsonAndroidVector);
      } else {
        reject("File can't be parsed");
      }
    }, (err) => {
      reject(err);
    });
  });
}

function prepareDirFiles (dirName, dirFiles) {
  const convertingPromises = [];
  _.forEach(dirFiles, (file) => {
    const path = iconsPath + `/${dirName}/${file.name}.svg`;
    convertingPromises.push(convertFileToAndroid(path, file.name));
  });
  return convertingPromises;
}

function generateSingleLibrary (dirData) {
  return new Promise((resolve) => {
    Promise.all(prepareDirFiles(dirData.name, dirData.files)).then((convertedIcons) => {
      const lib = {
        name  : dirData.name,
        icons : {},
      };
      _.forEach(convertedIcons, (icon) => {
        lib.icons[icon.name] = icon.svgData;
      });

      resolve(lib);
    });
  });
}

function generateMultipleLibraries (directoriesArr) {
  return new Promise((resolve) => {
    const dirCount = directoriesArr.length;
    const libs = [];
    const processNextDirectory = () => {
      generateSingleLibrary(directoriesArr[libs.length]).then((libData) => {
        // console.log(libData)
        libs.push(libData);
        if (dirCount > libs.length) {
          processNextDirectory();
        } else {
          resolve(libs);
        }
      });
    };
    processNextDirectory();
  });
}

function exportJsonLib (file, libData) {
  return new Promise((resolve, reject) => {
    jsonfile.writeFile(iconsPath +  file, libData, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('File created');
      }
    });
  });
}

function runScript () {
  converter.start().then(() => {
    getDirData().then((dirData) => {
      if (dirData.length > 0) {
        generateMultipleLibraries(dirData).then((libs) => {
          const libsToExport = libs.map((lib) => {
            return exportJsonLib(`/${lib.name}IconLib.json`, lib.icons);
          });
          Promise.all(libsToExport).then(() => {
            finishRun();
          });
        });
      } else {
        finishRun();
      }
    }).catch(() => {
      finishRun();
    });
  });
}

runScript();
