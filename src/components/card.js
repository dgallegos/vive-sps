import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import CardView from 'react-native-cardview';


const Card = ({ customStyles={}, onPress, title, date, image }) => (
  <TouchableOpacity 
    onPress={onPress}
    style={[styles.container, customStyles.container]}
    >
    {
      !image ?
      <Image 
        style={[styles.image, customStyles.image]}
        source={require('../assets/Image.png')}/> :
      <Image 
        style={[styles.image, customStyles.image]}
        source={{uri: `${image}`}}/>
    }
    <View style={[styles.textContainer, customStyles.textContainer]}>
      <Text style={[styles.title, customStyles.title]}>{title}</Text>
      <Text style={[styles.date, customStyles.date]}>{date}</Text>
    </View>
  </TouchableOpacity>
);

const CardAndroid = ({customStyles={}, onPress, title, date, image}) => (
  <TouchableOpacity 
    onPress={onPress}
    >
    <CardView
      style={[styles.container, customStyles.container]}
      cardElevation={2}
      cardMaxElevation={0.2}
    >
      {
        !image ?
        <Image 
          style={[styles.image, customStyles.image]}
          source={require('../assets/Image.png')}/> :
        <Image 
          style={[styles.image, customStyles.image]}
          source={{uri: `${image}`}}/>
      }
      <View style={[styles.textContainer, customStyles.textContainer]}>
        <Text style={[styles.title, customStyles.title]}>{title}</Text>
        <Text style={[styles.date, customStyles.date]}>{date}</Text>
      </View>
    </CardView>
  </TouchableOpacity>
);

const exportCard = Platform.OS == 'android' ? CardAndroid : Card

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row', 
    alignSelf: 'stretch', 
    backgroundColor: '#fff', 
    marginHorizontal: 20,
    marginVertical: 10,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.2
  },
  image: {
    width: 125, 
    height: 105
  },
  textContainer: {
    paddingHorizontal: 10,
    paddingTop: 5,
    width: 0,
    flexGrow: 1
  },
  title: {
    fontSize: 15,
    fontWeight: '500',
    color: '#8e8e8e',
    flex: 1
  },
  date: {
    fontSize: 11,
    color: '#8e8e8e',
    fontWeight: '500',
  }
})

export default exportCard