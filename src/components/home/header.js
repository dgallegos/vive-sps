import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import React from 'react';
import moment from 'moment';
// import SvgUri from 'react-native-svg-uri';

const climateSelector = {
  night: { bg: () => require('../../assets/climate/night.png') , color: '#fff', icon: () => require('../../assets/climate/moon.png')},
  day: { bg: () => require('../../assets/climate/day.png'), color: '#007CB4' , icon: () => require('../../assets/climate/sunny.png')},
  dayRain: { bg: () => require('../../assets/climate/rain.png'), color: '#007CB4' , icon: () => require('../../assets/climate/raining.png')},
  nightRain: { bg: () => require('../../assets/climate/night-rain.png'), color: '#fff' , icon: () => require('../../assets/climate/raining.png')}
}

const Header = ({ climate='day', temp}) => (
  <View style={styles.container}>
    <View style={styles.textContainer}>
      <View>
        <Text style={[styles.mainText, {color: climateSelector[climate].color}]}>San Pedro Sula</Text>
        <Text style={[styles.secondaryText, {color: climateSelector[climate].color}]}>Honduras</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 50, height: 50, marginRight: 20, marginBottom: 20}}
          source={climateSelector[climate].icon()}
        />
        <View style={{alignSelf: 'flex-end', marginRight: 5}}>
          <Text style={[styles.secondaryText, {color: climateSelector[climate].color, fontWeight: '500'}]}>{temp}ºC</Text>
          <Text style={[styles.secondaryText, {color: climateSelector[climate].color, fontSize: 12}]}>{moment().format('DD MMMM YYYY')}</Text>
        </View>
      </View>
    </View>
    <Image
      source={require('../../assets/smart-city.png')}
      style={styles.city}
    />
    <Image
      source={climateSelector[climate].bg()}
      style={styles.climateBackground}/>
  </View>
)

const styles = StyleSheet.create({
  container: {
    height: 235, 
    position: 'relative',
  },
  textContainer: {
    zIndex: 1, 
    flex: 1, 
    justifyContent: 'center', 
    marginLeft: 20, 
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginBottom: 20
  },
  mainText: {
    color: '#007CB4', 
    fontSize: 16, 
    fontWeight: 'bold'
  },
  secondaryText: {
    color: '#007CB4'
  },
  city: {
    zIndex: 1, 
    height: 120, 
    resizeMode: 'stretch', 
    width: Dimensions.get('window').width
  },
  climateBackground: {
    position: 'absolute', 
    top: 0, 
    bottom: 0, 
    left: 0, 
    right: 0,
    resizeMode: 'stretch',
    width: Dimensions.get('window').width
  }
});

export default Header;