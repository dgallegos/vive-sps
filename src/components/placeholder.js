import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Placeholder = ({ message='No hay nada que mostrar', load, lang }) => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text style={{color: '#AFB3BA', fontSize: 16}}>
      {message}
    </Text>
    {
      load && 
      <TouchableOpacity
        onPress={load}
        style={{backgroundColor: '#AFB3BA', marginTop: 10, borderRadius: 20}}
      >
        <Text style={{color: '#fff', padding: 10, fontSize: 14}}>{lang['Events.reload']}</Text>
      </TouchableOpacity>
    }
  </View>
);

export default Placeholder;