import React from 'react';
import { Svg,
  Path,
} from 'react-native-svg';

const SvgIcon = ({width, height, icon, color, opacity}) => (
  <Svg
    height={height}
    width={width}
    viewBox={`0 0 ${icon.viewBox[0]} ${icon.viewBox[1]}`}
    >
    {icon.paths.map((path, $i) =>
      <Path fillOpacity={opacity} fill={color} key={$i}  d={path}/>
    )}
  </Svg>
);

SvgIcon.propTypes = {
  width  : React.PropTypes.number.isRequired,
  height : React.PropTypes.number.isRequired,
  icon   : React.PropTypes.object.isRequired,
  color  : React.PropTypes.string,
};

export default SvgIcon;
