export default {
  Home: {
    experience: 'How would you like to experience the city?',
    discover: 'Discover',
    inform: 'Inform',
    inspire: 'Inspire',
    participate: 'Participate',
    activate: 'Activate',
    move: 'Move'
  },
  Events: {
    placeholderNextEvents: 'We are preparing more events for you.',
    placeholderHistoryEvents: 'Soon we\'ll write history',
    upcoming: 'Upcoming',
    recently: 'Recently',
    start: 'Date',
    location: 'Location',
    price: 'Price',
    description: 'Description',
    reload: 'Reload'
  },
  Discover: {
    hotelPhrase: 'A place to stay?',
    restaurantPhrase: 'Where to eat?',
    shoppingPhrase: 'Where to do shopping?',
    emergencyPhrase: 'Emergency Phones',
    entertainmentPhrase: 'A place to have fun?',
    nightlifePhrase: 'Nightlife',
    hotel: 'Hotels',
    restaurant: 'Restaurants',
    shopping: 'Shopping',
    emergency: 'Emergency Phones',
  },
  Places: {
    seeImages: 'See Images',
    images: 'Images',
    goBack: 'Go Back',
    imagesTitle: 'Venue Images'
  },
  Emergency: {
    call: 'Call'
  }
}