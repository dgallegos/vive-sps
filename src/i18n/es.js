export default {
  Home: {
    experience: '¿Cómo quieres experimentar la ciudad?',
    discover: 'Descubre',
    inform: 'Informate',
    inspire: 'Inspirate',
    participate: 'Participa',
    activate: 'Activate',
    move: 'Movilizate'
  },
  Events: {
    placeholderNextEvents: 'Estamos preparando mas eventos para ti',
    placeholderHistoryEvents: 'Pronto escribiremos la historia',
    upcoming: 'Proximamente',
    recently: 'Recientemente',
    start: 'Fecha',
    location: 'Lugar',
    price: 'Precio',
    description: 'Descripcion',
    reload: 'Refrescar'
  },
  Discover: {
    hotelPhrase: '¿Un lugar donde hospedarse?',
    restaurantPhrase: '¿Un lugar para comer?',
    shoppingPhrase: '¿Donde realizar compras?',
    emergencyPhrase: 'Telefonos de Emergencia',
    entertainmentPhrase: '¿Donde entretenerse?',
    nightlifePhrase: '¿Qué hacer de noche?',
    hotel: 'Hoteles',
    restaurant: 'Restaurantes',
    shopping: 'Compras',
    emergency: 'Telefonos de Emergencia',
  },
  Places: {
    seeImages: 'Ver Imagenes',
    images: 'Imagenes',
    goBack: 'Regresar',
    imagesTitle: 'Imagenes de Lugar'
  },
  Emergency: {
    call: 'Llamar'
  }
}