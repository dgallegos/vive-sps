import en from './en.js'
import es from './es.js'
import { flattenObject } from './helpers.js';

export default {
  'es': {
    ...flattenObject(es)
  },
  'en-US': {
    ...flattenObject(en)
  }
}

