const languages = [
  {
      code: 'es',
      name: 'Español'
  },
  {
      code: 'en-US',
      name: 'English (USA)'
  }
];

export default languages;