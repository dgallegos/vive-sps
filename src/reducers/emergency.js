import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

export const loadEmergencyPhones = createAction('LOAD_EMERGENCY_PHONES', apis.phones);
export const loadEmergencyCategory = createAction('LOAD_EMERGENCY_CATEGORY');

const reducer = handleActions({
  [loadEmergencyPhones]: (state, action) =>({
    ...state,
    phones: action.payload
  }),
  [loadEmergencyCategory]: (state, action) => ({
    ...state,
    categories: state.categories,
    isFetchingCategories: false
  })
}, {...initialState.emergency});

export default reducer;