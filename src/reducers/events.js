import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

export const fetchingEvents = createAction('FETCHING_EVENTS');
export const fetchingCategories = createAction('FETCHING_CATEGORIES');
export const loadByCategory = createAction('LOAD_BY_CATEGORY',apis.events);
export const loadEventCategories = createAction('LOAD_EVENT_CATEGORY', apis.eventCategories);

const reducer = handleActions({
  [loadByCategory]: (state, action) => ({
    ...state,
    [action.payload.category]: {
      ...(state[action.payload.category] || {}),
      [action.payload.type]: {
        events: action.payload.data,
        isFetching: false  
      }
    }
  }),
  [loadEventCategories]: (state, action) => ({
    ...state,
    categories: action.payload,
    isFetchingCategories: false
  }),
  [fetchingEvents]: (state, action) => ({
    ...state,
    [action.payload.category]: {
      ...(state[action.payload.category] || {}),
      [action.payload.type]: {
        isFetching: true
      }
    }
  }),
  [fetchingCategories]: (state, action) => ({
    ...state,
    isFetchingCategories: true
  })
}, {...initialState.events});

export default reducer;