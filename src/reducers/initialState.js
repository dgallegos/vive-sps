import moment from 'moment';
import SvgIcon from '../components/svgIcon.js';
import icons from '../assets/icons/svgIconLib.json';
import { Loc } from 'redux-react-native-i18n';

export default {
  menu: {
    items: [
      {name: 'discover',  screen: 'Categories', type: 'places,emergency', color: '#0EADE1', font: '#fff', menuBackgroundColor: '#F5FCFF'},
      {name: 'inform', screen: 'News',       type: 'news',  color: '#31BAE6', font: '#fff'},
      {name: 'inspire', screen: 'EventTabs',  type: 'events', category: 1, color: '#ABAAAF', font: '#fff'},
      {name: 'participate', screen: 'EventTabs',  type: 'events', category: 4, color: '#BBBABF', font: '#fff'},
      {name: 'activate',  screen: 'Categories', type: 'events,routes',  category: 2, route_category: 'recreational',color: '#E6E7EB', font: '#0EADE1', menuBackgroundColor: '#767f85'},
      {name: 'move',screen: 'Categories', type: 'routes,transportation', route_category: 'transportation', color: '#FEFEFE', font: '#0EADE1', menuBackgroundColor: '#e6e7eb'},
    ],
    now: moment()
  },
  places:{
    categories: [
      {
        id: 1, 
        name: 'hotel', 
        phrase: 'hotelPhrase', 
        category_id: '4bf58dd8d48988d1fa931735', 
        svg: icons['hospedaje'], 
        marker: 'hotel'
      },
      {
        id: 2, 
        name: 'restaurant', 
        phrase: 'restaurantPhrase', 
        category_id: '4d4b7105d754a06374d81259', 
        svg: icons['dinner'], 
        marker: 'restaurant'
      },
      {
        id: 3, 
        name: 'shopping', 
        phrase: 'shoppingPhrase', 
        category_id: '4bf58dd8d48988d1fd941735', 
        svg: icons['compras'],  
        marker: 'shopping'
      },
      {
        id: 4, 
        name: 'entertainment', 
        phrase: 'entertainmentPhrase', 
        category_id: '4bf58dd8d48988d1fd941735', 
        svg: icons['disco-ball-of-small-mirrors-with-musical-note-symbol'],  
        marker: 'entertainment'
      },
      {
        id: 5, 
        name: 'nightlife', 
        phrase: 'nightlifePhrase', 
        category_id: '4d4b7105d754a06376d81259', 
        svg: icons['bares'],  
        marker: 'nightlife'
      }
    ]
  },
  emergency: {
    categories: [
      {
        id: 4, 
        name: 'emergency', 
        phrase: 'emergencyPhrase', 
        svg: icons['alarm']
      }
    ],
    phones: []
  },
  events: {
    
  },
  routes: {
    categories: [],
    routes: [
      {name: 'Route'},
      {name: 'Route'}
    ]
  },
  transportation: {
    categories: [],
    companies: {}
  },
  news: {
    news: []
  }
}