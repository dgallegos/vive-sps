import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

const load = createAction('LOAD_MENU');
const loadWeather = createAction('LOAD_WEATHER', apis.weather);

const reducer = handleActions({
  [load]: (state, action) =>({
    ...state,
    items: state.items
  }),
  [loadWeather]: (state, action) => ({
    ...state,
    weather: action.payload
  })
}, {...initialState.menu});

export default reducer;
export { load, loadWeather };