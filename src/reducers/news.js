import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

const fetchingNews = createAction('FETCHING_NEWS');
const loadNews = createAction('LOAD_NEWS', apis.news);

const reducer = handleActions({
  [loadNews]: (state, action) =>({
    ...state,
    news: action.payload,
    isFetching: false
  }),
  [fetchingNews]: (state, action) => ({
    ...state,
    isFetching: true
  })
}, {...initialState.news});

export default reducer;
export { loadNews, fetchingNews }