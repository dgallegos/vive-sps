import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';
import cloneDeep from 'lodash/cloneDeep';

const loadPlaceCategories = createAction('LOAD_PLACE_CATEGORIES');
const loadPlacesByCategory = createAction('LOAD_PLACES_BY_CATEGORY', apis.places);
const loadVenuePhotos = createAction('LOAD_VENUE_PHOTOS', apis.venuePhotos);
const fetchingPhotos = createAction('FETCHING_PHOTOS');
const fetchingPlaceCategories = createAction('FETCHING_PLACE_CATEGORIES');
const fetchingPlacesByCategory = createAction('FETCHING_PLACES_BY_CATEGORY');

const reducer = handleActions({
  [loadPlaceCategories]: (state, action) =>({
    ...state,
    categories: state.categories,
    isFetchingCategories: false
  }),
  [loadPlacesByCategory]: (state, action) => ({
    ...state,
    places: {
      ...(state.places || {}),
      [action.payload.category]: action.payload.data
    }
  }),
  [fetchingPlaceCategories]: (state,action) => ({
    ...state,
    isFetchingCategories: true
  }),
  [loadVenuePhotos]: (state, action) => {
    const placesByCategory = cloneDeep(state.places[action.payload.category]);
    placesByCategory[action.payload.index].photos = action.payload.photos;
    
    return{
      ...state,
      places: {
        [action.payload.category]: placesByCategory
      }
    }
  }
}, {...initialState.places});

export default reducer;
export { loadPlacesByCategory, loadPlaceCategories, loadVenuePhotos }