import { combineReducers } from 'redux';
import menu from './menu.js';
import places from './places.js';
import routes from './routes.js';
import events from './events.js';
import transportation from './transportation.js';
import news from './news.js';
import emergency from './emergency.js';
import { i18nReducer } from 'redux-react-native-i18n';

const rootReducer = combineReducers({
  places,
  menu,
  routes,
  events,
  transportation,
  news,
  emergency,
  i18n: i18nReducer
});

export default rootReducer;
