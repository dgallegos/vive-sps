import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

const loadRouteCategories = createAction('LOAD_ROUTE_CATEGORIES', apis.routeCategories);
const loadRoutes = createAction('LOAD_ROUTES', apis.routes);
const fetchingRouteCategories = createAction('FETCHIN_ROUTE_CATEGORIES');

const reducer = handleActions({
  [loadRouteCategories]: (state, action) =>({
    ...state,
    categories: {
      ...state.categories,
      ...action.payload
    },
    isFetchingCategories: false
  }),
  [loadRoutes]: (state, action) => ({
    ...state,
    routes: {
      [action.payload.category]: action.payload.data
    },
  }),
  [fetchingRouteCategories]: (state, action) => ({
    ...state,
    isFetchingCategories: true
  })
}, {...initialState.routes});

export default reducer;
export { loadRouteCategories, loadRoutes };