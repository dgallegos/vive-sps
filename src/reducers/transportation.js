import { createAction, handleActions } from 'redux-actions';
import initialState from './initialState.js';
import apis from '../actions/apis.js';

export const loadTransportationCategories = createAction('LOAD_TRANSPORTATION_CATEGORIES', apis.routeCompaniesCategories);
export const loadTransportationCompanies = createAction('LOAD_TRANSPORTATION_COMPANIES', apis.transportationCompanies);

const reducer = handleActions({
  [loadTransportationCategories]: (state, action) => ({
    ...state,
    categories: action.payload
  }),
  [loadTransportationCompanies]: (state, action) => ({
    ...state,
    companies: {
      ...state.companies,
      [action.payload.category]: [
        ...action.payload.data
      ]
    }
  })
}, {...initialState.transportation});

export default reducer;