import React, {Component} from 'react';
import {View} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { registerScreens } from './screens.js';
import configureStore from './store/configureStore.js';
import i18n from './i18n/index.js';
import languages from './i18n/languages.js';
import { i18nActions } from 'redux-react-native-i18n';

const store = configureStore();

store.dispatch( i18nActions.setDictionaries( i18n ) );
store.dispatch( i18nActions.setLanguages( languages ) );
store.dispatch( i18nActions.setCurrentLanguage( 'es' ) );

registerScreens(store, Provider);

class App extends Component{
  constructor(props) {
    super(props);
    this.startApp();
  }

  startApp() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'Home',
        title: 'Home',
        navigatorStyle: {},
        navigatorButtons: {},
      },
      passProps: {},
      animationType: 'slide-down'
    });
  }
}

export default App