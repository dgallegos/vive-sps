import { Navigation } from 'react-native-navigation';

import Home from './screens/Home.js';
import Events from './screens/Events.js';
import EventsHistory from './screens/EventsHistory.js';
import EventDetail from './screens/EventDetail.js';
import EventHistory from './screens/EventHistory.js';
import Places from './screens/Places.js';
import PlaceDetail from './screens/PlaceDetail.js';
import Categories from './screens/Categories.js';
import Routes from './screens/Routes.js';
import RouteDetail from './screens/RouteDetail.js';
import News from './screens/News.js';
import NewsDetail from './screens/NewsDetail.js';
import EventTabs from './screens/EventTabs.js';
import Transportation from './screens/Transportation.js';
import EmergencyPhones from './screens/EmergencyPhones.js';
import ImageGallery from './screens/ImageGallery.js';
import PlaceImages from './screens/PlaceImages.js';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('Home', () => Home, store, Provider);
  Navigation.registerComponent('Events', () => Events, store, Provider);
  Navigation.registerComponent('EventsHistory', () => EventsHistory, store, Provider);
  Navigation.registerComponent('EventTabs', () => EventTabs, store, Provider);
  Navigation.registerComponent('News', () => News, store, Provider);
  Navigation.registerComponent('Places', () => Places, store, Provider);
  Navigation.registerComponent('Categories', () => Categories, store, Provider);
  Navigation.registerComponent('NewsDetail', () => NewsDetail, store, Provider);
  Navigation.registerComponent('EventDetail', () => EventDetail, store, Provider);
  Navigation.registerComponent('EventHistory', () => EventHistory, store, Provider);
  Navigation.registerComponent('PlaceDetail', () => PlaceDetail, store, Provider);
  Navigation.registerComponent('Routes', () => Routes, store, Provider);
  Navigation.registerComponent('RouteDetail', () => RouteDetail, store, Provider);
  Navigation.registerComponent('ImageGallery', () => ImageGallery, store, Provider);
  Navigation.registerComponent('Transportation', () => Transportation, store, Provider);
  Navigation.registerComponent('EmergencyPhones', () => EmergencyPhones, store, Provider);
  Navigation.registerComponent('PlaceImages', () => PlaceImages, store, Provider);
}