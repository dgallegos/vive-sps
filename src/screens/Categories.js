import React, { Component } from 'react';
import { View, Text, Image, Dimensions, FlatList, TouchableOpacity, StyleSheet, Platform } from 'react-native';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadPlaceCategories } from '../reducers/places.js';
import { loadRouteCategories } from '../reducers/routes.js';
import { loadTransportationCategories } from '../reducers/transportation.js';
import { loadEventCategories } from '../reducers/events.js';
import { loadEmergencyCategory } from '../reducers/emergency.js';
// Libs
import cloneDeep from 'lodash/cloneDeep';
import { url } from '../actions/apis.js';
// UI
import CardView from 'react-native-cardview';
import SvgUri from '../lib/svgUri.js';
import SvgIcon from '../components/svgIcon.js';
import icons from '../assets/icons/svgIconLib.json';
import Spinner  from 'react-native-spinkit';
import Placeholder from '../components/placeholder.js';
import { Loc } from 'redux-react-native-i18n';

const nextScreen = {
  events: 'EventTabs',
  routes: 'Routes',
  places: 'Places',
  transportation: 'Transportation',
  emergency: 'EmergencyPhones'
};

const categoryActions = {
  places: {
    action: actions => actions.loadPlaceCategories(),
  },
  events: {
    action: (actions, param) => actions.loadEventCategories(param),
    parameter: 'category'
  },
  routes: {
    action: (actions, param) => actions.loadRouteCategories(param),
    parameter: 'route_category'
  },
  transportation: {
    action: (actions, param) => actions.loadTransportationCategories(param),
    parameter: 'route_category'
  },
  emergency: {
    action: actions => actions.loadEmergencyCategory()
  }
}

const banner = {
  discover: () => require('../assets/Descubre.png'),
  activate: () => require('../assets/Activate.png'),
  move: () => require('../assets/Movilizate.png')
};

class Categories extends Component{
  constructor(){
    super();
    this.renderItem = this.renderItem.bind(this);
    this.renderItemAndroid = this.renderItemAndroid.bind(this);
    this.getCategories = this.getCategories.bind(this);
    this.onPress = this.onPress.bind(this);
    this.state = {
      categories: []
    }
  }

  componentDidMount(){
    const { menuProps, actions,  } = this.props;
    const typeArray = menuProps.type.split(',');
    
    Promise
      .all(typeArray.map( t => {
        return categoryActions[t].parameter ? 
          categoryActions[t].action(actions, menuProps[categoryActions[t].parameter]) : 
          categoryActions[t].action(actions);
      }))
  }

  getCategories(){
    const { reduxState, menuProps } = this.props;
    const state = cloneDeep(reduxState);

    return Object.keys(reduxState).reduce( (prev, curr) => {
      if(curr == 'events'){
        prev.push({ name: menuProps.name , type: curr, svg: icons[menuProps.name.toLowerCase()]})
      }else if (curr == 'routes'){
        const categories = state[curr].categories[menuProps.route_category] || [];
        categories.forEach(category => category.type = 'routes')
        prev.push(...categories);
      }
      else{
        let categories = [...state[curr].categories];
        categories.forEach(category => category.type = curr)
        prev.push(...categories)
      }
      return prev;
    }, []);
  }

  onPress(item){
    const { currentLanguage, dictionaries } = this.props.i18n;
    console.log(item)
    this.props.navigator.push({
      screen: nextScreen[item.type],
      title: item.phrase ? dictionaries[currentLanguage][`Discover.${item.name}`] : item.name[0].toUpperCase() + item.name.slice(1),
      backButtonTitle: '',
      passProps: {
        item,
        menuProps: this.props.menuProps
      }
    });
  }

  getIcon(item){
    if(item.icon){
      return (
        <SvgUri
          width="37"
          height="31"
          fill="#9B9CA0"
          source={{uri: `${url}/${item.icon}`}}
        />
      )
    }

    if(item.svg){
      return (
        <SvgIcon
          icon={item.svg}
          height={31}
          width={37}
          color='#000'
          opacity={0.2}
        />
      );
    }

    return(
      <Image 
        source={require('../assets/006-photo-camera.png')}
        style={{height: 31, width: 37, opacity: 0.2}}
      />
    );
  }

  renderItemAndroid({item, index}){
    const { currentLanguage } = this.props.i18n;
    const { color, font, name } = this.props.menuProps;
    return(
      <View style={{backgroundColor: color}}>
        <CardView
          style={{flex: 1, margin: 0, padding: 0, backgroundColor: color}}
          cardElevation={4}
          cardMaxElevation={0.5}
        >
          <TouchableOpacity 
            style={[styles.listItem, {backgroundColor: color}]}
            onPress={() => this.onPress( item )}
          >
            <View style={{flex: 1}}>
              <Text style={{color: font, fontWeight: '500'}}>
                { 
                  item.phrase ? 
                    <Loc locKey={`Discover.${item.phrase}`}/> :
                    (
                      item.name == 'activate' ? 
                      <Loc locKey={`Home.${item.name}`}/> :
                      ( 
                        currentLanguage == 'es' ? 
                        item.name : 
                        item.name_english
                      )
                    )
                }
              </Text>
            </View>
            {this.getIcon(item)}
          </TouchableOpacity>
        </CardView>
      </View>
    )
  }

  renderItem({item, index}){
    const { currentLanguage } = this.props.i18n;
    const { color, font, name } = this.props.menuProps;
    return(
      <TouchableOpacity 
        style={[styles.listItem, {backgroundColor: color}]}
        onPress={() => this.onPress( item )}
      >
        <View style={{flex: 1}}>
          <Text style={{color: font, fontWeight: '500'}}>
            { 
              item.phrase ? 
                <Loc locKey={`Discover.${item.phrase}`}/> :
                (
                  item.name == 'activate' ? 
                  <Loc locKey={`Home.${item.name}`}/> :
                  ( 
                    currentLanguage == 'es' ? 
                    item.name : 
                    item.name_english
                  )
                )
            }
          </Text>
        </View>
        <View >
        {this.getIcon(item)}
        </View>
      </TouchableOpacity>
    )
  }

  renderLoading(){
    const { menuProps } = this.props;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner 
          isVisible={true} 
          size={50} 
          type={'9CubeGrid'} 
          color={menuProps.color}
        />
      </View>
    );
  }

  render(){
    const { menuBackgroundColor, color, name, type } = this.props.menuProps;
    const isFetching = type
      .split(',')
      .map(type => this.props.reduxState[type].isFetchingCategories)
      .reduce((prev, curr) => prev && curr, true);

    if(isFetching){
      return this.renderLoading();
    }
    
    const categories = this.getCategories();
    
    const renderItem = Platform.OS == 'android' ? this.renderItemAndroid : this.renderItem
    return(
      <View style={[styles.container, {backgroundColor: menuBackgroundColor}]}>
        <Image
          style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
          source={name ? banner[name]() : (() => require('../assets/Image-bg.png'))()}
        />
        {
          categories.length ? 
          <FlatList
            data={categories}
            renderItem={renderItem}
            keyExtractor={ (_, idx) => idx }
          /> : 
          <Placeholder
            message='Ya regresamos con mas!'
            color={color}
          />
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    flexDirection: 'row',
    width: Dimensions.get('window').width, 
    height: 60, 
    backgroundColor: '#fff', 
    alignItems: 'center', 
    paddingLeft: 28,
    paddingRight: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.5,
  }
});

const categorySelector = (state, ownProps) =>{
  const { type } = ownProps.menuProps;
  const allTypes = type.split(',');
  
  return { 
    reduxState: allTypes.reduce(
      (prev, curr) => {
        prev[curr] = state[curr]
        return prev;
      }, 
      {}
    ),
    i18n: state.i18n
  };
};

export default connect(
  categorySelector,
  dispatch => ({
    actions: bindActionCreators({ 
      loadPlaceCategories, 
      loadRouteCategories,
      loadEventCategories, 
      loadTransportationCategories,
      loadEmergencyCategory
    }, dispatch)
  })
)(Categories)




