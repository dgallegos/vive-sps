import React, { Component } from 'react';
import { View, Text, Image, FlatList, Dimensions, StyleSheet , TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadEmergencyPhones } from '../reducers/emergency.js';
import CardView from 'react-native-cardview';
import { phonecall } from 'react-native-communications';
import { Loc } from 'redux-react-native-i18n';

class Emergency extends Component{
  static navigatorStyle={
    navBarBackgroundColor: '#e86a5a'
  }

  constructor(){
    super();
    this.renderItem = this.renderItem.bind(this);
    this.renderItemAndroid = this.renderItemAndroid.bind(this);
    this.onPress = this.onPress.bind(this);
  }

  componentDidMount(){
    const { actions } = this.props;
    actions.loadEmergencyPhones().then(console.log);
  }

  onPress(item){
    
  }

  renderItemAndroid({item}){
    const { color, font, name } = this.props.menuProps;
    return(
      <View style={{backgroundColor: '#eeeeee'}}>
        <CardView
            style={{flex: 1, margin: 0, padding: 0, backgroundColor: '#eeeeee'}}
            cardElevation={4}
            cardMaxElevation={0.5}
          >
          <View 
            style={[styles.listItem, {backgroundColor: '#eeeeee'}]}
          >
            <View style={{flex: 1}}>
              <Text style={{color: '#e86a5a', fontWeight: '500'}}>{ item.name }</Text>
            </View>
            <TouchableOpacity 
              style={{backgroundColor: '#e86a5a', paddingVertical: 8, paddingHorizontal: 20, borderRadius: 20}}
              onPress={() => phonecall(String(item.phone), true)}
            >
              <Text style={{color: '#fff', fontSize: 10}}>
                <Loc locKey={`Emergency.call`}/>
              </Text>
            </TouchableOpacity>
          </View>
        </CardView>
      </View>
    )
  }

  renderItem({item}){
    const { color, font, name } = this.props.menuProps;
    
    return(
      <View 
        style={[styles.listItem, {backgroundColor: '#eeeeee'}]}
      >
        <View style={{flex: 1}}>
          <Text style={{color: '#e86a5a', fontWeight: '500'}}>{ item.name }</Text>
        </View>
        <TouchableOpacity 
          style={{backgroundColor: '#e86a5a', paddingVertical: 8, paddingHorizontal: 20, borderRadius: 20}}
          onPress={() => phonecall(String(item.phone), false)}
        >
          <Text style={{color: '#fff', fontSize: 10}}>
            <Loc locKey={`Emergency.call`}/>
          </Text>
        </TouchableOpacity>
      </View>
    )
  }

  render(){
    const { menuBackgroundColor } = this.props.menuProps;
    const { phones } = this.props.emergency;
    const renderItem = Platform.OS == 'android' ? this.renderItemAndroid : this.renderItem
    return(
      <View style={[styles.container, {backgroundColor: '#fff'}]}>
        <Image
          style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
          source={require('../assets/Tel.png')}
        />
        <FlatList
          data={phones}
          renderItem={renderItem}
          keyExtractor={ (_, idx) => idx }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    flexDirection: 'row',
    width: Dimensions.get('window').width, 
    height: 60, 
    backgroundColor: '#fff', 
    alignItems: 'center', 
    paddingLeft: 28,
    paddingRight: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.5
  }
});

export default connect(
  state => ({
    emergency: state.emergency,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({ loadEmergencyPhones }, dispatch)
  })
)(Emergency);



