import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, ScrollView } from 'react-native';
import { url } from '../actions/apis.js';
import moment from 'moment';

const EVENT_DATA = [
  {field: 'start', selector: v => moment(v).format('DD/MM/YYYY')}, 
  {field: 'location'}, 
  {field: 'price', selector: price => price == '0' ? 'Gratis' : price}, 
  {field: 'description'}
];

const EventDetail = ({ item, dict }) => (
  <View style={{flex: 1, backgroundColor: '#fff'}}>
    <Image
      style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
      source={{uri: `${url}${item.main_image}`}}
    />
    <ScrollView style={styles.container}>
      <Text style={styles.eventTitle}>{item.name}</Text>
      {
        EVENT_DATA.map( (data, key) => 
          <View key={key} style={styles.section}>
            <Text style={styles.sectionTitle}>{dict[`Events.${data.field}`]}</Text>
            <Text style={styles.sectionText}>{data.selector ? data.selector(item[data.field]): item[data.field]}</Text>
          </View>
        )
      }
    </ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
    marginVertical: 20
  },
  eventTitle: {
    fontSize: 20, 
    fontWeight: 'bold', 
    color: '#000',
    marginBottom: 10
  },
  section: {
    marginVertical: 5
  },
  sectionTitle: {
    color: '#696969',
    fontSize: 12,
    fontWeight: 'bold'
  },
  sectionText: {
    color: '#4d4d4d',
    fontSize: 12,
    paddingVertical: 5   
  }
})


export default EventDetail;
