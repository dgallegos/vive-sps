import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { url } from '../actions/apis.js';

export default class EventHistory extends Component{
  constructor(props){
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  onPress(key){
    const { item } = this.props;
    this.props.navigator.push({
      screen: 'ImageGallery',
      title: 'Inspirate',
      backButtonTitle: '',
      passProps:{
        images: item.images,
        currentIndex: key
      }
    })
  }

  render(){
    const { item } = this.props;
    return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Image
          style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
          source={{uri: `${url}${item.main_image}`}}
        />
        <View style={styles.container}>
          <Text style={styles.eventTitle}>{item.name}</Text>
          <Text style={styles.subtitle}>Galeria de Fotos</Text>
        </View>

        <ScrollView>
          <View style={{ justifyContent: 'center', flexDirection: 'row', flexWrap: 'wrap' }}>
          { 
            item.images.map( (image,key) =>
              <TouchableOpacity
                key={key}
                onPress={() => this.onPress(key)}
              >
                <Image
                  
                  style={{margin: 10, width: 90, height: 60, backgroundColor: '#000'}}
                  source={{uri: `${url}${image.path}`}}
                />
              </TouchableOpacity>
            )
          }
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
    marginVertical: 20
  },
  eventTitle: {
    fontSize: 20, 
    fontWeight: 'bold', 
    color: '#000',
    marginBottom: 10
  },
  subtitle: {
    fontSize: 14, 
    fontWeight: 'bold', 
    color: '#000'
  },
  section: {
    marginVertical: 5
  },
  sectionTitle: {
    color: '#696969',
    fontSize: 12,
    fontWeight: 'bold'
  },
  sectionText: {
    color: '#4d4d4d',
    fontSize: 12,
    paddingVertical: 5   
  }
});