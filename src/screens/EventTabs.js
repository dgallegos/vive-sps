import React, { Component } from 'react';
import { View, Text } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Events from './Events';
import EventsHistory from './EventsHistory';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class EventTabs extends Component{
  render(){
    const { color, i18n } = this.props;
    const { currentLanguage, dictionaries } = i18n;
    return (
      <ScrollableTabView
        tabBarBackgroundColor='#eee'
        tabBarActiveTextColor={'#9f9f9f'}
        tabBarInactiveTextColor='#BDBBC8'
        tabBarUnderlineStyle={{backgroundColor: '#9f9f9f'}}
      >
        <Events tabLabel={ dictionaries[currentLanguage]['Events.upcoming'] } {...this.props} />
        <Events tabLabel={ dictionaries[currentLanguage]['Events.recently'] } {...this.props} history/>
      </ScrollableTabView>
    )
  }
}

export default connect(
  state => ({
    i18n: state.i18n
  })
)(EventTabs)
