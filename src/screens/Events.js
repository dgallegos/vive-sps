import React, { Component } from 'react';
import { View, Text, FlatList, Platform, RefreshControl } from 'react-native';
import Card from '../components/card.js';
import Placeholder from '../components/placeholder.js';
import Spinner  from 'react-native-spinkit';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadByCategory, fetchingEvents } from '../reducers/events.js';
// Libs
import moment from 'moment';
import { url } from '../actions/apis.js';

class Events extends Component{
  constructor(){
    super();
    this.onPress = this.onPress.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.getEvents = this.getEvents.bind(this);
    this.state = {
      isRefreshing: false
    }
  }

  componentDidMount(){
    this.getEvents();
  }

  getEvents(){
    const { actions, menuProps, tabLabel, history } = this.props;
    actions.fetchingEvents({
      category: menuProps.category, 
      type: history ? 'history' : 'current'
    });
    return actions.loadByCategory(menuProps.category, history);
  }

  onRefresh(){
    this.setState({isRefreshing: true});
    this.getEvents()
    .then(()=>{
      this.setState({isRefreshing: false});
    })
  }

  onPressAndroid(item){
    const { currentLanguage, dictionaries } = this.props.i18n;
    this.props.navigator.showModal({
      screen: this.props.history ? 'EventHistory' : 'EventDetail', 
      title: item.name,
      backButtonTitle: '',
      passProps: {
        item,
        dict: dictionaries[currentLanguage]
      },
      style: {
        zIndex: 20
      },
      animationType: 'slide-up'
    });
  }

  onPress(item){
    const { currentLanguage, dictionaries } = this.props.i18n;
    this.props.navigator.push({
      screen: this.props.history ? 'EventHistory' : 'EventDetail', 
      title: item.name,
      backButtonTitle: '',
      passProps: {
        item,
        dict: dictionaries[currentLanguage]
      }
    });
  }

  renderItem({item, index}){
    return(
      <Card
        title={item.name}
        date={moment(item.start).format('DD/MM/YYYY')}
        image={`${url}${item.main_image}`}
        onPress={() =>  Platform.OS =='android' ? this.onPressAndroid(item) : this.onPress(item)}
        customStyles={{
          container: {
            marginTop: index == 0 ? 20 : 10,
            padding: 5,
            backgroundColor: '#fff'
          },
          image: {
            height: 80,
            width: 110,
            marginBottom: Platform.OS =='android' ? 3 : 0
          },
          textContainer:{
            flex: 1,
            marginLeft: 15,
          },
          title: {
            color: '#000',
            fontSize: 13,
          },
          date: {
            marginTop: 5,
            marginBottom: 5
          }
        }}
      />
    );
  }

  renderLoading(){
    const { menuProps } = this.props;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner 
          isVisible={true} 
          size={50} 
          type={'9CubeGrid'} 
          color={menuProps.color}
        />
      </View>
    );
  }

  render(){
    const { menuProps, events, tabLabel, history, i18n } = this.props;
    const { currentLanguage, dictionaries } = i18n;
    const t = history ? 'history' : 'current' ;
    const isFetching = events[menuProps.category] ? (events[menuProps.category][t] ? events[menuProps.category][t].isFetching : true) : true
    
    if(isFetching){
      return this.renderLoading();
    }

    const data = events[menuProps.category] ? (events[menuProps.category][t] ? events[menuProps.category][t].events : []) : []; 
    return(
      <View style={{flex: 1}}>
        {
          data.length ?
          <FlatList
            data={data}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.id}
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh}
          />:
          <Placeholder 
            message={t == 'current' ? dictionaries[currentLanguage]['Events.placeholderNextEvents'] : dictionaries[currentLanguage]['Events.placeholderHistoryEvents']}
            color={menuProps.color}
            load={this.getEvents}
            lang={dictionaries[currentLanguage]}
          /> 
        }
      </View>
    )
  }
}

export default connect(
  state => ({
    events: state.events,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({loadByCategory, fetchingEvents}, dispatch)
  })
)(Events);

