import React, { Component } from 'react';
import { View, Text, FlatList, Platform } from 'react-native';
import Card from '../components/card.js';
import Placeholder from '../components/placeholder.js';
import Spinner  from 'react-native-spinkit';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadByCategory, fetchingEvents } from '../reducers/events.js';
// Libs
import moment from 'moment';
import { url } from '../actions/apis.js';

class EventsHistory extends Component{
  constructor(){
    super();
    this.onPress = this.onPress.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount(){
    const { actions, menuProps, tabLabel } = this.props;
    actions.fetchingEvents({
      category: menuProps.category, 
      type: 'history'
    });
    actions.loadByCategory(menuProps.category, tabLabel);
  }

  onPress(item){
    this.props.navigator.push({
      screen: 'EventHistory', 
      title: item.name,
      backButtonTitle: '',
      passProps: {
        item
      }
    });
  }

  renderItem({item, index}){
    return(
      <Card
        title={item.name}
        date={moment(item.start).format('DD/MM/YYYY')}
        image={`${url}${item.main_image}`}
        onPress={() => this.onPress(item)}
        customStyles={{
          container: {
            marginTop: index == 0 ? 20 : 10,
            padding: 5,
            backgroundColor: '#fff'
          },
          image: {
            height: 80,
            width: 110,
            marginBottom: Platform.OS =='android' ? 3 : 0
          },
          textContainer:{
            flex: 1,
            marginLeft: 15,
          },
          title: {
            color: '#000',
            fontSize: 13,
          },
          date: {
            marginTop: 5,
            marginBottom: 5
          }
        }}
      />
    );
  }

  renderLoading(){
    const { menuProps } = this.props;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner 
          isVisible={true} 
          size={50} 
          type={'9CubeGrid'} 
          color={menuProps.color}
        />
      </View>
    );
  }

  render(){
    const { menuProps, events, tabLabel } = this.props;
    const t = 'history';
    const isFetching = events[menuProps.category] ? (events[menuProps.category][t] ? events[menuProps.category][t].isFetching : true) : true
    
    if(isFetching){
      return this.renderLoading();
    }

    const data = events[menuProps.category] ? (events[menuProps.category][t] ? events[menuProps.category][t].events : []) : []; 
    return(
      <View style={{flex: 1}}>
        {
          data.length ?
          <FlatList
            data={data}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.id}
          />:
          <Placeholder 
            message={t == 'current' ? 'Estamos preparando mas eventos para ti' : 'Historial de eventos'}
            color={menuProps.color}
          /> 
        }
      </View>
    )
  }
}

export default connect(
  state => ({events: state.events}),
  dispatch => ({
    actions: bindActionCreators({loadByCategory, fetchingEvents}, dispatch)
  })
)(EventsHistory);

