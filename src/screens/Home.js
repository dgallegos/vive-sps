import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  FlatList, 
  TouchableOpacity, 
  Dimensions,
  Image 
} 
from 'react-native';
import { connect } from 'react-redux';
import { load, loadWeather } from '../reducers/menu.js';
import { bindActionCreators } from 'redux';
import Header from '../components/home/header.js';
import moment from 'moment';
import SvgIcon from '../components/svgIcon.js';
import icons from '../assets/icons/svgIconLib.json'
import { Loc } from 'redux-react-native-i18n';

const i = {
  discover: icons['photo-camera'],
  inform: icons['folded-newspaper'],
  inspire: icons['light-bulb'],
  participate: icons['man-standing-with-arms-up'],
  activate: icons['cyclist'],
  move: icons['man-walking']
};

const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

class Home extends Component {
  static navigatorStyle = {
    navBarHidden: true
  }

  constructor(props) {
    super(props);
    this.renderItem = this.renderItem.bind(this);
    this.state = {
      weather: 'day',
      temp: 0
    }
  }

  componentDidMount(){
    const { actions } = this.props;
    actions.load();
    actions.loadWeather()
      .then( ({action, payload}) => {
        const hour = moment().format('H');
        let weather = hour > 18 ? 'night' : 'day';
        weather = String(payload.cod).startsWith(8) ? weather : `${weather}Rain`;

        this.setState({
          weather,
          temp: parseInt(payload.temp),
        });
      })
      .catch( err => console.log(err));
  }

  keyExtractor(item, index){
    return index
  }

  renderItem({item}){
    const { currentLanguage, dictionaries } = this.props.i18n;

    return(
      <TouchableOpacity 
        style={[styles.listItem, {backgroundColor: item.color}]}
        onPress={() => this.props.navigator.push({
          screen: item.screen,
          title: dictionaries[currentLanguage][`Home.${item.name}`],
          backButtonTitle: '',
          navigatorStyle: {
            navBarBackgroundColor: item.color,
            navBarTextColor: item.font,
            navBarButtonColor: item.font,
            backButtonTitle: ''
          },
          passProps: {
            menuProps: {...item}
          }
        })}
      >
        <View style={{flex: 1}}>
          <Text style={{color: item.font, fontWeight: '500'}}>
            <Loc locKey={`Home.${item.name}`}/>
          </Text>
        </View>
        <SvgIcon
          icon={i[item.name]}
          height={31}
          width={37}
          color='#000'
          opacity={0.2}
        />
      </TouchableOpacity>
    )
  }

  render() {
    const { i18n } = this.props;
    const { items, now } = this.props.menu;
    const { weather, temp } = this.state;
    return (
      <View style={styles.container}>
        <Header climate={weather} temp={temp}/>
        <View style={{alignSelf: 'stretch', justifyContent: 'center', height: 75, backgroundColor: '#fff'}}>
          <Text style={{textAlign: 'center', color: '#007CB4', fontWeight: '500'}}>
            <Loc locKey={'Home.experience'}/>
          </Text>
        </View>
        <FlatList
          data={items}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    elevation: 5,
    flexDirection: 'row',
    width: Dimensions.get('window').width, 
    height: x > 400 ? 72 : 60,
    backgroundColor: '#fff', 
    alignItems: 'center', 
    paddingLeft: 28,
    paddingRight: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,

  }
});

export default connect(
  state => ({
    menu: state.menu,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({load, loadWeather}, dispatch)
  })
)(Home);

