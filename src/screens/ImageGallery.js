import React, {Component} from 'react';
import { View, Text, Image, ScrollView, Dimensions } from 'react-native';
import PageControl from 'react-native-page-control';
import { url } from '../actions/apis.js';

export default class ImageGallery extends Component{
  constructor(){
    super();
    this.onPageControlValueChange = this.onPageControlValueChange.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onClickButton = this.onClickButton.bind(this);
    this.state = {
      currentPage: 0,
      width: Dimensions.get('window').width
    }
  }

  componentDidMount(){
    this.setState({
      currentPage: this.props.currentIndex
    });
  }

  onPageControlValueChange(currentPage) {
    this.refs.ScrollView.scrollResponderScrollTo({x: this.state.width * currentPage, y: 0, animated: true});
  }

  onClickButton(){
    const currentPage = this.state.currentPage + 1;
    if(currentPage == Pages.length){
      Actions.login({type: ActionConst.RESET})
    }else{
      this.onPageControlValueChange(currentPage);
    }
  }

  onScroll({nativeEvent}) {
    let currentPage = Math.round(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
    if (this.state.currentPage !== currentPage) {
      this.setState({currentPage});
    }
  }

  render(){
    const { images } = this.props;
    return (
      <View style={{flex: 1, backgroundColor: '#686868', justifyContent: 'center', alignItems: 'center'}}>
        <ScrollView 
          ref="ScrollView"
          horizontal={true}
          onScroll={this.onScroll}
          pagingEnabled={true}
          contentContainerStyle={{alignItems:'center', justifyContent: 'center'}}
          scrollEventThrottle={16}>
          {
            images.map((image, key) => 
              <Image
                key={key}
                style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 300}}
                source={{uri: `${url}${image.path}`}}
              />
            )
          }
        </ScrollView>
        <PageControl 
          style={{position: 'absolute', left: 0, bottom: 30, right: 0}}
          numberOfPages={images.length}
          currentPage={this.state.currentPage}
          hidesForSinglePage={true}
          pageIndicatorTintColor='#dafdb7'
          currentPageIndicatorTintColor='#dafdb7'
          indicatorStyle={{borderRadius: 5, opacity: 0.2}} 
          currentIndicatorStyle={{borderRadius: 5}}
          indicatorSize={{width:8, height:8}}
          onPageIndicatorPress={this.onPageControlValueChange} 
        />
      </View>
    )
  }
}
