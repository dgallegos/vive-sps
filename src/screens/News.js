import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
// UI Componenets
import Card from '../components/card.js';
import { connect } from 'react-redux';
import Spinner  from 'react-native-spinkit';
import Placeholder from '../components/placeholder.js';
// Redux
import { loadNews, fetchingNews } from '../reducers/news.js';
import { bindActionCreators } from 'redux';
// Libs
import moment from 'moment';
import { url } from '../actions/apis.js';

class News extends Component{
  constructor(){
    super();
    this.onPress = this.onPress.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.getNews = this.getNews.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.state = {
      isRefreshing: false
    }
  }

  componentDidMount(){
    this.getNews();
  }

  getNews(){
    const { actions, menuProps, tabLabel, history } = this.props;
    actions.fetchingNews();
    return actions.loadNews();
  }

  onRefresh(){
    this.setState({isRefreshing: true});
    this.getNews()
    .then(()=>{
      this.setState({isRefreshing: false});
    })
  }

  onPress(item){
    const { currentLanguage, dictionaries } = this.props.i18n;
    this.props.navigator.push({
      screen: 'NewsDetail',
      title: dictionaries[currentLanguage][`Discover.inform`],
      passProps: {
        news: item,
        lang: currentLanguage
      }
    });
  }

  renderItem({item, index}){
    const { currentLanguage } = this.props.i18n;
    return(
      <Card
        key={index}
        title={currentLanguage == 'es' ? 
                        item.title : 
                        item.title_english}
        date={moment(item.created_at).format('DD/MM/YYYY')}
        image={item.images.length ? `${url}${item.images[0].path}` : undefined}
        onPress={() => this.onPress(item)}
        customStyles={{
          container: {
            marginTop: index == 0 ? 20 : 10
          }
        }}
      />
    );
  }

  renderLoading(){
    const { menuProps } = this.props;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Spinner 
          isVisible={true} 
          size={50} 
          type={'9CubeGrid'} 
          color={menuProps.color}
        />
      </View>
    );
  }

  render(){
    const { menuProps, i18n } = this.props;
    const { currentLanguage, dictionaries } = i18n;
    const { news, isFetching } = this.props.news;

    if(isFetching){
      return this.renderLoading();
    }

    return(
      <View style={{backgroundColor: '#f7f7f7', flex: 1}}>
        {
          news.length ? 
          <FlatList
            data={news}
            renderItem={this.renderItem}
            keyExtractor={ (item, index) => item.id}
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh}
          /> : 
          <Placeholder 
            message={'Nuevas noticias pronto'}
            color={menuProps.color}
            load={this.getNews}
            lang={dictionaries[currentLanguage]}
          /> 
        }
      </View>
    );
  }
}

export default connect(
  state => ({
    news: state.news,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({loadNews, fetchingNews }, dispatch)
  })
)(News);

