import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet, ScrollView, Platform, WebView } from 'react-native';
import { url } from '../actions/apis.js';
import HTML from 'react-native-render-html'
import Swiper from 'react-native-swiper';

const convertHTML = (content, color, backgroundColor, fontSize) => {
  let css =`
  <style type="text/css">
      body {
        font-family: "Gotham";
        color: ${color};
        background-color: ${backgroundColor};
        font-size:${fontSize};
        text-align: justify;
      }
      p {
        font-size: 12px;
      }

    </style>
  `;
  return `${css} ${content}`
}

const injectedJavaScript =  backgroundColor => (
  `document.body.style.backgroundColor = ${backgroundColor};`
);

const NewsDetail = ({ news, lang }) => (
  <View style={{flex: 1}}>
    <Swiper
      autoplay
      autoplayTimeout={4}
      showsPagination={false}
      height={248}>
      {
        news.images.map((image, key) => (
          <Image
            key={key}
            style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 230}}
            source={{uri: `${url}${image.path}`}}
          />
        ))
      }
    </Swiper>
    {
      Platform.OS == 'android' ? 
        <View style={{flex: 1, margin: 15}}>
          <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'center'}}>
            {lang == 'es' ? news.title : news.title_english}
          </Text>
          <WebView 
            style={{flex: 1, marginTop: 5, backgroundColor: "#E8E8E8"}}
            automaticallyAdjustContentInsets={true}
            injectedJavaScript={injectedJavaScript("#7a7f84")}
            source={{html: lang == 'es' ? 
              convertHTML(news.content, "#7a7f84", "#FAFAFA", 20) : 
              convertHTML(news.content_english, "#7a7f84", '#FAFAFA', 20)
            }}
          />
        </View> : 
        <ScrollView style={{margin: 25}}>
          <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'justify'}}>
            {lang == 'es' ? news.title : news.title_english}
          </Text>
          <HTML
            html={lang == 'es' ? news.content : news.content_english}
            htmlStyles={styles}
          />
        </ScrollView>
      }
  </View>
);

const bodyColor = "#7a7f84";
const styles = StyleSheet.create({
  div:{
    marginHorizontal:10,
    textAlign: 'auto',
  },
  strong:{
    color:bodyColor
  },
  p:{
    color:bodyColor,
    textAlign: 'justify',
  },
  li:{
    color:bodyColor
  },
  blockquote:{
    color:bodyColor
  }
});

export default NewsDetail;