import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StyleSheet } from 'react-native';


export default class PlaceDetail extends Component{
  render(){
    const { item } = this.props;
    const loc = item.location;
    const address = `${loc.crossStreet || ''} ${loc.address || ''}`;
    const url = item.photos.items ? 
      ( item.photos.items.length ? `${item.photos.items[0].prefix}320x260${item.photos.items[0].suffix}` : undefined ) : 
      undefined;
    return(
      <View >
        {
          url ? 
          <Image
            style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
            source={{uri: url}}
          /> : 
          <Image
            style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
            source={require('../assets/Image-bg.png')}
          />
        }
        <View style={styles.container}>
          <Text style={styles.placeTitle}>{item.name}</Text>
          <Text>{address}</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 30,
    marginVertical: 20
  },
  placeTitle: {
    fontSize: 20, 
    fontWeight: 'bold', 
    color: '#000',
    marginBottom: 10
  },
  section: {
    marginVertical: 5
  },
  sectionTitle: {
    color: '#696969',
    fontSize: 12,
    fontWeight: 'bold'
  },
  sectionText: {
    color: '#4d4d4d',
    fontSize: 12,
    paddingVertical: 5   
  }
})


