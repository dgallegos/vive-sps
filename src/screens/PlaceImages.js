import React, {Component} from 'react';
import { View, Text, Image, Dimensions, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Swiper from 'react-native-swiper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadVenuePhotos } from '../reducers/places.js';
import { url } from '../actions/apis.js';
import { Loc } from 'redux-react-native-i18n';

class PlaceImages extends Component{
  componentDidMount(){
    const { category, index, place, actions } = this.props;
    actions.loadVenuePhotos(place.id, category, index);
  }

  dismissLightBox(){
    this.props.navigator.dismissLightBox();
  }

  render(){
    const { places, category, index, place } = this.props;
    const photos = places.places[category][index].photos;
    return(
      <View style={[{flex: 1},  Platform.OS == 'android' ? styles.containerAndroid : {} ]}>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{place.name}</Text>
          <Text style={styles.subtitle}>
            <Loc locKey={`Places.images`}/>
          </Text>
        </View>
        <Swiper
          autoplay
          autoplayTimeout={4}
          showsPagination={false}
          height={248}>
          {
             photos ? 
              photos.map((image, key) => (
                <Image
                  key={key}
                  style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 230}}
                  source={{uri: `${image.prefix}320x260${image.suffix}`}}
                />
              )) : 
              <View>
                <Text>
                  No Images
                </Text>
              </View>
          }
        </Swiper>
        <View style={styles.textContainer}>
          <Text style={styles.subtitle}>{photos ? photos.length : 0} Imagenes</Text>
        </View>
        {
          Platform.OS == 'ios' && 
          <TouchableOpacity
            onPress={() => this.dismissLightBox()}
            style={styles.button}
          >
            <Text style={{padding: 15, color: '#fff'}}> 
              <Loc locKey={`Places.goBack`}/>
            </Text>
          </TouchableOpacity>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    marginHorizontal: 20,
    marginBottom: 40
  },
  title: {
    color: '#fff', 
    fontSize: 20, 
  },
  subtitle: {
    color: '#fff', 
    fontSize: 14
  },
  button: {
    backgroundColor: '#A28FC3', 
    width: 200, 
    alignItems: 'center', 
    justifyContent: 'center', 
    alignSelf: 'center', 
    borderRadius: 20,
    marginTop: 20
  },
  containerAndroid: {
    backgroundColor: '#BBBABF',
    justifyContent: 'center'
  }
})

export default connect(
  state => ({
    places: state.places,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({ 
      loadVenuePhotos
    }, dispatch)
  })
)(PlaceImages);
