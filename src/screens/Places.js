import React, { Component } from 'react';
import { View, Text, FlatList, Platform, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import Card from '../components/card.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadPlacesByCategory } from '../reducers/places.js';
import MapView from 'react-native-maps';
import { Loc } from 'redux-react-native-i18n';

const markerImages = {
  hotel: () => require('../assets/mall.png'),
  restaurant: () => require('../assets/comida.png'),
  shopping: () => require('../assets/supermercado.png'),
  entertainment: () => require('../assets/teatro.png'),
  nightlife: () => require('../assets/bares.png')
}

class Places extends Component{
  constructor(){
    super();
    this.onPress = this.onPress.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.state = {
      place: {}
    }
  }

  componentDidMount(){
    const { actions, item } = this.props;
    actions.loadPlacesByCategory(item.category_id);
  }

  setCurrentPlace(place, index){
    this.setState({
      place,
      index
    });
  }

  showImages(category){
    const { place, index } = this.state;
    const { currentLanguage, dictionaries } = this.props.i18n;
    Platform.OS =='android' ?
      this.props.navigator.showModal({
        screen: "PlaceImages",
        title: dictionaries[currentLanguage]['Places.imagesTitle'], 
        passProps: {
          place, 
          index,
          category
        }
      }) :
      this.props.navigator.showLightBox({
        screen: "PlaceImages", 
        passProps: {
          place, 
          index,
          category
        }, 
        style: {
          backgroundBlur: "none", 
          backgroundColor: "#9C9DA380", 
          tapBackgroundToDismiss: true 
        }
      })
  }

  onPress(item){
    this.props.navigator.push({
      screen: 'PlaceDetail',
      passProps: {
        item
      }
    });
  }

  renderItem({item, index}){
    const loc = item.location;
    const address = `${loc.crossStreet || ''} ${loc.address || ''}`;
    const url = item.photos.items ? 
      ( item.photos.items.length ? `${item.photos.items[0].prefix}125x105${item.photos.items[0].suffix}` : undefined ) : 
      undefined;
    return(
      <Card
        title={item.name}
        date={address}
        image={url}
        onPress={() => this.onPress(item)}
        customStyles={{
          container: {
            marginTop: index == 0 ? 20 : 10,
            padding: 5,
            backgroundColor: '#fff'
          },
          image: {
            height: 80,
            width: 110,
            marginBottom: Platform.OS =='android' ? 3 : 0
          },
          textContainer:{
            flex: 1,
            marginLeft: 15,
          },
          title: {
            color: '#000',
            fontSize: 13,
            flex: 0
          },
          date: {
            marginTop: 5
          }
        }}
      />
    );
  }

  render(){
    const { places } = this.props.places;
    const { category_id, marker: markerImage } = this.props.item; 
    const markers = category_id && places ? places[category_id] :[];

    return(       
      <View style={stylesMap.container}>
        <MapView
          style={stylesMap.map}
          initialRegion={{
            latitude: 15.5,
            longitude: -88.035,
            latitudeDelta: 0.03,
            longitudeDelta: 0.03,
          }}
          zoomEnabled 
          scrollEnabled
        >
          {
            markers && markers.map((marker, key) => 
              <MapView.Marker
                key={key}
                coordinate={{latitude: marker.location.lat, longitude: marker.location.lng}}
                title={marker.name}
                description={`${marker.location.address || ''} ${marker.location.crossStreet || ''}`}
                onPress={() => this.setCurrentPlace(marker, key)}
              >
                <Image
                  source={markerImages[markerImage]()}
                  style={{width: 35, height: 40}}
                />
              </MapView.Marker>
            )
          }
        </MapView>
        {
          this.state.place.name &&
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => this.showImages(category_id)}
              style={[styles.bubble, styles.button]}
            >
              <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                <Loc locKey={`Places.seeImages`}/>
              </Text>
              <Text style={{ fontSize: 10, fontWeight: 'bold' }}>{this.state.place.name}</Text>
            </TouchableOpacity>
          </View>
        }
      </View>
    );
  }
}


const stylesMap = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
});

const styles = StyleSheet.create({
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 150,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
})

export default connect(
  state => ({
    places: state.places,
    i18n: state.i18n
  }),
  dispatch => ({
    actions: bindActionCreators({ 
      loadPlacesByCategory
    }, dispatch)
  })
)(Places);



