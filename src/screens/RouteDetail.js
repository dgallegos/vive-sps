import React, { Component } from 'react';
import { View, Text, Dimensions, Platform } from 'react-native';
import { url } from '../actions/apis.js';
import PhotoView from 'react-native-photo-view';
import Image from 'react-native-image-zoom'

const { width, height } = Dimensions.get('window');

export default class RouteDetail extends Component{
  render(){
    const { route_image } = this.props.item;
    return(
      <View style={{flex: 1, padding: 0}}>
        <PhotoView
          source={{uri: `${url}${route_image}`}}
          minimumZoomScale={0.1}
          maximumZoomScale={3}
          style={{padding: 0, flex: 1, width, height, resizeMode: 'contain'}}
        />
      </View>
    )
  }
}


