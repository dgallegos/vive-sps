import React, { Component } from 'react';
import { View, Text, Image, FlatList, Dimensions, StyleSheet , TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadRoutes } from '../reducers/routes.js';
import CardView from 'react-native-cardview';
import { url } from '../actions/apis.js';

class Routes extends Component{
  constructor(){
    super();
    this.renderItem = this.renderItem.bind(this);
    this.renderItemAndroid = this.renderItemAndroid.bind(this);
    this.onPress = this.onPress.bind(this);
  }

  componentDidMount(){
    const { id } = this.props.item;
    this.props.actions.loadRoutes(id);
  }

  onPress(item){
    this.props.navigator.push({
      screen: 'RouteDetail',
      title: item.name,
      backButtonTitle: '',
      passProps: {
        item
      }
    });
  }

  renderItemAndroid({item, index}){
    const { color, font, name } = this.props.menuProps;
    return(
      <View style={{backgroundColor: color}}>
        <CardView
          style={{flex: 1, margin: 0, padding: 0, backgroundColor: color}}
          cardElevation={4}
          cardMaxElevation={0.5}
        >
          <TouchableOpacity 
            style={[styles.listItem, {backgroundColor: color}]}
            onPress={() => this.onPress( item )}
          >
            <View style={{flex: 1}}>
              <Text style={{color: font, fontWeight: '500'}}>{ item.phrase || item.name }</Text>
            </View>
          </TouchableOpacity>
        </CardView>
      </View>
    )
  }

  renderItem({item, index}){
    const { color, font, name } = this.props.menuProps;
    return(
      <TouchableOpacity 
        style={[styles.listItem, {backgroundColor: color}]}
        onPress={() => this.onPress( item )}
      >
        <View style={{flex: 1}}>
          <Text style={{color: font, fontWeight: '500'}}>{ item.name }</Text>
        </View>
      </TouchableOpacity>
    )
  }


  render(){
    const { menuBackgroundColor } = this.props.menuProps;
    const { routes } = this.props.routes;
    const { id, banner_image } = this.props.item;
    const renderItem = Platform.OS == 'android' ? this.renderItemAndroid : this.renderItem
    return(
      <View style={[styles.container, {backgroundColor: menuBackgroundColor}]}>
        {
          banner_image ? 
          <Image
            style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
            source={{uri: `${url}${banner_image}`}}
          /> : 
          <Image
            style={{resizeMode: 'stretch', width: Dimensions.get('window').width, height: 260}}
            source={require('../assets/Image-bg.png')}
          />
        }
        <FlatList
          data={id ? routes[id] : []}
          renderItem={renderItem}
          keyExtractor={ (_, idx) => idx }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    flexDirection: 'row',
    width: Dimensions.get('window').width, 
    height: 60, 
    backgroundColor: '#fff', 
    alignItems: 'center', 
    paddingLeft: 28,
    paddingRight: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.5
  }
});

export default connect(
  state => ({
    routes: state.routes
  }),
  dispatch => ({
    actions: bindActionCreators({ loadRoutes }, dispatch)
  })
)(Routes);



