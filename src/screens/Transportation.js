import React, { Component } from 'react';
import { View, Text, Image, FlatList, Dimensions, StyleSheet , TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadTransportationCompanies } from '../reducers/transportation.js';
import CardView from 'react-native-cardview';
import { phonecall } from 'react-native-communications';

class Transportation extends Component{
  constructor(){
    super();
    this.renderItem = this.renderItem.bind(this);
    this.renderItemAndroid = this.renderItemAndroid.bind(this);
  }

  componentDidMount(){
    const { id } = this.props.item;
    this.props.actions.loadTransportationCompanies(id)
      .then(console.log);
  }

  renderItemAndroid({item}){
    const { color, font, name } = this.props.menuProps;
    return(
      <View style={{backgroundColor: color}}>
        <CardView
          style={{flex: 1, margin: 0, padding: 0, backgroundColor: color}}
          cardElevation={4}
          cardMaxElevation={0.5}
        >
          <View 
            style={[styles.listItem, {backgroundColor: color}]}
          >
            <View style={{flex: 1}}>
              <Text style={{color: font, fontWeight: '500'}}>{ item.name }</Text>
            </View>
            <TouchableOpacity 
              style={{backgroundColor: font, paddingVertical: 8, paddingHorizontal: 20, borderRadius: 20}}
              onPress={() => phonecall(String(item.phone), true)}
            >
              <Text style={{color: color, fontSize: 10}}>Llamar</Text>
            </TouchableOpacity>
          </View>
        </CardView>
      </View>
    )
  }

  renderItem({item}){
    const { color, font, name } = this.props.menuProps;
    return(
      <View 
        style={[styles.listItem, {backgroundColor: color}]}
      >
        <View style={{flex: 1}}>
          <Text style={{color: font, fontWeight: '500'}}>{ item.name }</Text>
        </View>
        <TouchableOpacity 
          style={{backgroundColor: font, paddingVertical: 8, paddingHorizontal: 20, borderRadius: 20}}
          onPress={() => phonecall(String(item.phone), false)}
        >
          <Text style={{color: color, fontSize: 10}}>Llamar</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render(){
    const { menuBackgroundColor } = this.props.menuProps;
    const { companies } = this.props.transportation;
    const { id } = this.props.item;
    const renderItem = Platform.OS == 'android' ? this.renderItemAndroid : this.renderItem
    return(
      <View style={[styles.container, {backgroundColor: menuBackgroundColor}]}>
        <FlatList
          data={id ? companies[id]: []}
          renderItem={renderItem}
          keyExtractor={ (_, idx) => idx }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  listItem: {
    flexDirection: 'row',
    width: Dimensions.get('window').width, 
    height: 60, 
    backgroundColor: '#fff', 
    alignItems: 'center', 
    paddingLeft: 28,
    paddingRight: 22,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.5
  }
});

export default connect(
  state => ({
    transportation: state.transportation
  }),
  dispatch => ({
    actions: bindActionCreators({ loadTransportationCompanies }, dispatch)
  })
)(Transportation);



